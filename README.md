# PippyPlatform??? #

Yeah, I named this after myself initially out of fun because I didn't expect this to turn out as good as it did.
I was trying to think of a way I could centralise all my coding in one nice little location to make accessing them simpler and eventually thought of a way in which they are able to communicate by means of shared modules.
The platform is not really useful on its own but when developing using the templates included in the download section you will be able to easily create tools that will simply integrate with the platform.

The beauty of this is that the way it is designed, applications designed with the template may *also* run as a standalone application (Doesn't require the PippyPlatform, but doesn't get the centralised benefits like shared modules etc.)

### What is this repository for? ###

* PippyPlatform - Centralised module-based development
* PippyPlatformLibrary - Shared assembly which needs to be added to each integrated application for compatibility

### How do I get set up? ###

* Download the source to a location and compile.
* Place applications into the folder "/plugins/applications/applicationName"
* Restart the PippyPlatform.
* Click on "Applications" on the very top left of the application (Thanks to Mahapps.Metro I can do weird things like this :P)

### Contribution guidelines ###

* Most of the functionality is complete in the sense that what I need it to do will work.
* System modules while included don't function at all as it turned out I didn't need them
* Modify which ever parts you see fit, I really don't mind.
* Commonly used classes which are to be shared betwen the Platform and the applications should go into the "Pippy Platform Library" which is a project in a folder inside the root folder.

