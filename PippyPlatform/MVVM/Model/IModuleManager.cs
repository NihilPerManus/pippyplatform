﻿using System.Collections.Generic;
using PippyPlatform.MVVM.Service.PippyModule.Module;

namespace PippyPlatform.MVVM.Model
{
    interface IModuleManager
    {
        IEnumerable<IModule> Modules { get; set; }
    }
}