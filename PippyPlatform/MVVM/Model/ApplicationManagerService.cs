﻿using PippyPlatform.MVVM.Service.PippyView.View;
using PippyPlatform.MVVM.Service.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.MVVM.Model
{
    //This class is used for the UI. It is injected into the constructor of the ViewModel. Keeps things tidy this does.
    class ApplicationManagerService : Observable.ObservableClass, IApplicationManager
    {
        private IEnumerable<IPippyViewCategory> _Categories;

        public IEnumerable<IPippyViewCategory> Categories { get { return _Categories; } set { _Categories = value;  RaisePropertyChanged(); } } 

        public ApplicationManagerService(IEnumerable<IPippyViewCategory> viewCollection) //These come from te view provider.
        {
            Categories = viewCollection;
        }
    }
}
