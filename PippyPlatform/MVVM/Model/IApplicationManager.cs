﻿using System.Collections.Generic;
using PippyPlatform.MVVM.Service.PippyView.View;

namespace PippyPlatform.MVVM.Model
{
    interface IApplicationManager
    {
        IEnumerable<IPippyViewCategory> Categories { get; set; }
    }
}