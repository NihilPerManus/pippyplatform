﻿using PippyPlatform.MVVM.Service.PippyModule.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.MVVM.Model
{
    class ModuleManagerService : IModuleManager
    {
        //I'm using thisto hold the modules because the constructor seemed to be getting a bit full.
        //There's lots about how many things should be in a constructor.
        //While generally I don't care, I did this to tidy things up a bit.
        public IEnumerable<IModule>Modules { get; set; }
        public ModuleManagerService(IEnumerable<IModule> modules)
        {
            Modules = modules;
        }
    }
}
