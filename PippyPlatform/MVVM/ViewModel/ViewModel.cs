﻿using PippyPlatform.Attributes;
using PippyPlatform.BaseClass;
using PippyPlatform.Command;
using PippyPlatform.MVVM.Model;
using PippyPlatform.MVVM.Service;
using PippyPlatform.MVVM.Service.AssemblyLoader;
using PippyPlatform.MVVM.Service.PippyModule;
using PippyPlatform.MVVM.Service.PippyModule.Module;
using PippyPlatform.MVVM.Service.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace PippyPlatform.MVVM.ViewModel
{
    class ViewModel : PippyClass, IViewModel, INotifyPropertyChanged
    {
        
        private Dictionary<string, List<IView>> _Views = new Dictionary<string, List<IView>>();
        private IView _SelectedView;
        private bool _ShowApplicationFlyout;
        private bool _ShowModulesFlyout;
  
        private IModuleManager _ModuleManager;
        private IApplicationManager _ApplicationManager;

        public IModuleManager ModuleManager { get { return _ModuleManager; } private set { _ModuleManager = value; RaisePropertyChanged(); } }
        public IApplicationManager ApplicationManager { get { return _ApplicationManager; } private set { _ApplicationManager = value; RaisePropertyChanged(); } }
        public IView SelectedView { get { return _SelectedView; } set { _SelectedView = value; RaisePropertyChanged(); } }
        public ICommand TextChanged { get { return new DelegateCommand(e => Console.WriteLine(e + " changed!")); } }
    

        public bool ShowApplicationFlyout { get { return _ShowApplicationFlyout; } set { _ShowApplicationFlyout = value; if(value) ShowModulesFlyout = false ; RaisePropertyChanged(); } }
        public bool ShowModulesFlyout { get { return _ShowModulesFlyout; } set { _ShowModulesFlyout = value; if (value) ShowApplicationFlyout = false; RaisePropertyChanged(); } }
         
        public ViewModel( IModuleManager moduleManager, IApplicationManager applicationManager)
        {
            //get the views with their categories



            ApplicationManager = applicationManager;
            ModuleManager = moduleManager;

          //  Console.WriteLine("APPS => " + AppDomain.CurrentDomain.GetAssemblies().Count());
            
        }

        public ICommand ShowViewEvent => new DelegateCommand(e => ShowView(e));

        
        //Passing a view to this command, if everything is ok, this will actively set the visible application to the front
        private void ShowView(object e)
        {
            try
            {
                var view = (e as IView);
                //If it was allowed to run
                if (view.Enabled)
                {
                    //If there is something selected then set it as the selected view
                    if(view.MainView != null)
                        SelectedView = view;

                    //hide the flyout
                    ShowApplicationFlyout = false;
                }
                else
                {
                    log.Info($"{view.MainView.Title} is disabled");
                }

            }
            catch
            {

                log.Debug($"Failed to load . Error with dependencies! ");
            }
        }

        //VIEW UPDATING FUNCTIONS - DO NOT REMOVE (Like anything really)//
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName]string name = "")
        {
         //raise a property has change to the UI so it knows to update   
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        //////////////////////////////////////////////////////////////////
        
   

    }
}
