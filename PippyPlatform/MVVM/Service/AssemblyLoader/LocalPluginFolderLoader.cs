﻿using PippyPlatform._CompositionRoot;
using PippyPlatform.Attributes;
using PippyPlatform.BaseClass;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;



namespace PippyPlatform.MVVM.Service.AssemblyLoader
{
    //These classes are used to inject and extract the various assemblies
    class LocalPluginFolderLoader : BaseAssemblyLoaderModel, IAssemblyLoaderModel
    {
        public LocalPluginFolderLoader()
        {
            PluginFolder = new DirectoryInfo(Directory.GetCurrentDirectory() + @"\plugins\applications");
        }

        //Gets the assemblues found in the specified folder. It will then inject a shared container into the assembly which in turn will fill it with its container.
        //As it is a class, the container will 
        public void Load(ref Container container)
        {
            if (!PluginFolder.Exists)
                PluginFolder.Create();
            //Get all the types stored within the .Exe. This is what allows us to get the info from a standalone application
            IEnumerable<Type> types = GetAssembliesFromExtensions(".exe")
                .Select(t => t.EntryPoint.DeclaringType) //get the entry point and find the one with an attribute
                .Where(t => GetPippyAttribute<PippyTypeAttribute>(t) != null);

            foreach(Type t in types)
            {
                log.Debug("Found Entry point: " + t);
                try
                {
                    //try to create an instance of it and inject our container into it trying to get the registrations out
                    Activator.CreateInstance(t, container);
                }
                catch (MissingMethodException)
                {
                    throw new MissingMethodException($"Ensure {t} has a constructor with a ref Container Constructor. \nIf it does, a common cause is that you are using a different version of simple injector");
                }
            }
                


        }
    }
    class LocalModuleFolderLoader : BaseAssemblyLoaderModel, IAssemblyLoaderModel
    {
        //Get all the modules
        public LocalModuleFolderLoader()
        {
            PluginFolder = new DirectoryInfo(Directory.GetCurrentDirectory() + @"\plugins\modules");
        }

        //To laod them need to find if they exist.
        public void Load(ref Container container)
        {
            //create the folder if it doesn't exist
            if (!PluginFolder.Exists)
                PluginFolder.Create();

            //Get all the assemblies which are in DLLs
            var types = GetAssembliesFromExtensions(".dll")
                ?.Select(t =>
                    t.GetExportedTypes() //for every type in the current DLL
                        .Where(_t => GetPippyAttribute<PippyTypeAttribute>(_t) != null && GetPippyAttribute<PippyTypeAttribute>(_t).Type == "MODULE")
                            ?.FirstOrDefault()); //Get the first class whose Type is "MODULE"

            foreach (Type t in types)
            {
                if(t != null)
                {
                    //When you find the compositionroot activate it.
                    log.Debug("found composition root => " + t);

                    //THe creating of the instance incluses placing the container in the constructor.
                    //This is what dives into the container and fishes out the registrations
                    Activator.CreateInstance(t, container);
                }
                
            }

        }
    }
    class LocalSystemFolderLoader : BaseAssemblyLoaderModel, IAssemblyLoaderModel
    {

        public LocalSystemFolderLoader()
        {
            PluginFolder = new DirectoryInfo(Directory.GetCurrentDirectory() + @"\plugins\system");
        }

        public void Load(ref Container container)
        {
        
        }

        //Gets the assemblues found in the specified folder. It will then inject a shared container into the assembly which in turn will fill it with its container.
        //As it is a class, the container will 



    }


    abstract class BaseAssemblyLoaderModel : PippyClass
    {
        protected DirectoryInfo PluginFolder;
        public Container SharedContainer { get; set; }

        //provide a simple way of getting the attribute of a class. It was so annoying to fully write out each time
        protected T GetPippyAttribute<T>(Type fromType) where T : Attribute
        {
            return (Attribute.GetCustomAttribute(fromType, typeof(T)) as T);
        }

        protected IEnumerable<Assembly> GetAssembliesFromExtensions(string extension)
        {
            //Gets all assemblies that are not this program.
            return PluginFolder.GetDirectories()
               ?.SelectMany(fld => fld.GetFiles()?.Where(f => f.Name.EndsWith(extension)))
               ?.Select(f => Assembly.LoadFrom(f.FullName));
        }


    }
}



