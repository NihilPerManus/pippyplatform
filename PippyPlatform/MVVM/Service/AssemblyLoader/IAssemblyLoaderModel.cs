﻿using System.Collections.Generic;
using System.Windows.Controls;
using SimpleInjector;

namespace PippyPlatform.MVVM.Service.AssemblyLoader
{
    public interface IAssemblyLoaderModel
    {
        void Load(ref Container container);
    }
}