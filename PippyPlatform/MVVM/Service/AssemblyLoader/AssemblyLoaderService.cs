﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PippyPlatform.MVVM.Service.AssemblyLoader
{
    class AssemblyLoaderService : IAssemblyLoaderService
    {
        private IAssemblyLoaderModel _AssemblyLoader;

        public AssemblyLoaderService(IAssemblyLoaderModel assemblyLoader)
        {
            _AssemblyLoader = assemblyLoader;
        }

        public void Load(ref Container container)
        {
            _AssemblyLoader.Load(ref container);
        }

    }
}
