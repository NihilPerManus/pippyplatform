﻿using SimpleInjector;
using System.Collections.Generic;
using System.Windows.Controls;

namespace PippyPlatform.MVVM.Service.AssemblyLoader
{
    public interface IAssemblyLoaderService
    {
        void Load(ref Container container);

    }
}