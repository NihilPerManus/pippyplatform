﻿using System.Collections.Generic;
using PippyPlatform.MVVM.Service.PippyModule.Module;

namespace PippyPlatform.MVVM.Service.PippyModule
{
    interface IPippyModuleProvider
    {
        IModule Provide(IModule moduleRegistration);
    }
}