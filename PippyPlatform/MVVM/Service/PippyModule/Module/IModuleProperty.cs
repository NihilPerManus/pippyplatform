﻿using System;
using System.Reflection;

namespace PippyPlatform.MVVM.Service.PippyModule.Module
{
    public interface IModuleProperty
    {
        PropertyInfo Property { get; set; }
        Type PropertyType { get;  }
        string Name { get; }
        object BaseObject { get; set; }
        object Value { get; set; }
    }
}