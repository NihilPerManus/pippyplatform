﻿using System.Collections.Generic;
using System.Reflection;

namespace PippyPlatform.MVVM.Service.PippyModule.Module
{
    public interface IModule
    {
        object BaseClass { get; set; }
        string Name { get; set; }
        List<IModuleProperty> Properties { get; set; }
    }
}