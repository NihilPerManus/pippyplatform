﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.MVVM.Service.PippyModule.Module
{
    //What's in a module?
    class Module : IModule
    {
        //The base class which is what the model is supposed to do
        public object BaseClass { get; set; }
        //The name of the module
        public string Name { get; set; }
        //the properties associated with the object.
        public List<IModuleProperty> Properties { get; set; } = new List<IModuleProperty>();
    }
}
