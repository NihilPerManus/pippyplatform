﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.MVVM.Service.PippyModule.Module
{
    class ModuleProperty : IModuleProperty
    {
        private object _BaseObject;
        private PropertyInfo _Property;

        public object BaseObject
        {
            get
            {
                return _BaseObject;
            }

            set
            {
                _BaseObject = value;
            }
        }

        public string Name
        {
            get
            {
                return Property.Name;
            }
        }

        public PropertyInfo Property
        {
            get
            {
                return _Property;
            }

            set
            {
                _Property = value;
            }
        }

        public Type PropertyType
        {
            get
            {
                return Property.PropertyType; 
            }
        }

        public object Value
        {
            get
            {
                object _Value = Property.GetValue(BaseObject, null);
                //no need for further processing
                //if (PropertyType == typeof(string) && _Value == null) return "";
                return _Value;
            }

            set
            {
              
                Property.SetValue(BaseObject, value);
            }
        }
    }
}
