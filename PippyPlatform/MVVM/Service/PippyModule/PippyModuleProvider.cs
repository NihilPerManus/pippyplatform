﻿using PippyPlatform.BaseClass;
using PippyPlatform.MVVM.Service.PippyModule.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.MVVM.Service.PippyModule
{
    class PippyModuleProvider : PippyClass, IPippyModuleProvider
    {
        private Func<IModule> _moduleFactory;
        private Func<IModuleProperty> _modulePropertyFactory;
      

        public PippyModuleProvider(Func<IModule> moduleFactory, Func<IModuleProperty> modulePropertyFactory)
        {
           //Back when I could be bothered writing factories (pointless in this kind of app)
            _moduleFactory = moduleFactory;
            _modulePropertyFactory = modulePropertyFactory;
        }

        //completes an IModule which is only given a baseobject
        public IModule Provide(IModule moduleRegistration)
        {
            //Get an empty IModule from the factory
            IModule module = _moduleFactory?.Invoke();
            log.Debug("PROPERTY TIME");
            log.Debug(moduleRegistration.BaseClass.GetType());
            //Get a list of all the properties
            moduleRegistration.BaseClass.GetType().GetProperties()
            .ToList()
            .ForEach(p => 
            {
                var modProp = _modulePropertyFactory?.Invoke();
                //transfer the original baseclass to the new one
                modProp.BaseObject = moduleRegistration.BaseClass;
                //Create the property
                modProp.Property = p;
                //Add the property to the new item we created
                moduleRegistration.Properties.Add(modProp);
                       
            });

            //Write out each property for debugging purposes
            moduleRegistration.Properties.ForEach(p => Console.WriteLine(p.Name));

            //Next we get the name of the project which was typed in the attribute of the original application/module
            moduleRegistration.Name = (Attribute.GetCustomAttribute(moduleRegistration.BaseClass.GetType(), typeof(Attributes.PippyModuleNameAttribute)) as Attributes.PippyModuleNameAttribute).Name;

            //return the new and improved module
            return moduleRegistration;
               
        }

    }
}
