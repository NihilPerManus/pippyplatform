﻿using System.Collections.Generic;
using PippyPlatform.MVVM.Service.View;

namespace PippyPlatform.MVVM.Service.PippyView.View
{
    public interface IPippyViewCategory
    {
        string Name { get; set; }
        IEnumerable<IView> Views { get; set; }
    }
}