﻿using PippyPlatform.MVVM.Service.PippyView.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PippyPlatform.MVVM.Service.View
{
    //this is a state class used to just hold state. no processing
    class PippyView : IView
    {
        public IPippyViewCategory Category { get; set; }

        public bool Enabled { get; set; }

        public Page MainView { get; set; }
    }
}
