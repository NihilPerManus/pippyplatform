﻿using PippyPlatform.MVVM.Service.PippyView.View;
using System.Windows.Controls;

namespace PippyPlatform.MVVM.Service.View
{
    public interface IView
    {
        IPippyViewCategory Category { get; set; }
        Page MainView { get; set; }
        bool Enabled { get; set;}

    }
}