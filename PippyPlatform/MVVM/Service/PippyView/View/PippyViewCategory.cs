﻿using PippyPlatform.MVVM.Service.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.MVVM.Service.PippyView.View
{
    //The views that are contained by a category. This is better handled in Compendium
    public class PippyViewCategory : IPippyViewCategory
    {
        public string Name { get; set; }
        public IEnumerable<IView> Views { get; set; }
    }
}
