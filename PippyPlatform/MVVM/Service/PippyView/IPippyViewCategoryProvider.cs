﻿using PippyPlatform.MVVM.Service.PippyView.View;
using PippyPlatform.MVVM.Service.View;
using System.Collections.Generic;

namespace PippyPlatform.MVVM.Service
{
    public interface IPippyViewCategoryProvider
    {
        IEnumerable<IPippyViewCategory> GetCategories(IEnumerable<IView> ViewSource);
        
    }
}