﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PippyPlatform.MVVM.Service.View;
using PippyPlatform.Attributes;
using PippyPlatform.MVVM.Service.PippyView.View;
using PippyPlatform.BaseClass;

namespace PippyPlatform.MVVM.Service.PippyView
{
    class PippyViewProvider : PippyClass, IPippyViewProvier
    {

        Func<IPippyViewCategory> _categoryFactory;
        public PippyViewProvider(Func<IPippyViewCategory> categoryFactory)
        {
            _categoryFactory = categoryFactory;
        }

        public IView Provide(IView viewRegistration)
        {
            
            var attribute = GetPippyAttribute<PippyCategoryAttribute>(viewRegistration.MainView.GetType()); //Get the category from its attribute. This is a requirement. Without it, it is skipped.

            if (attribute != null) //Does the attribute exist?
            {
                //log.Debug($"Category [{attribute.Category}] found for {page.Title}");

                viewRegistration.Category = _categoryFactory?.Invoke();
                viewRegistration.Category.Name = attribute.Category; //extract the category and put it in the object


                bool enable = true; //default true, then run a gauntlet to see if there is a single false 

                //log.Debug($"{page.Title} attribute testing begins...");
                Attribute.GetCustomAttributes(viewRegistration.MainView.GetType())
                .Where(req => req.GetType().GetInterfaces().Contains(typeof(IRequirement))) //Get all attributes of an application which have a requirement
                .Select(req => req as IRequirement)
                .ToList()
                .ForEach(req =>
                {

                    //Test each attribute against the assembly. It just takes one fail to stop loading it in particular. (This will set a flag for the UI to grey it out :) )
                    log.Debug("Testing [" + req.Value + "] on requirement: [" + req + "]");
                    if (!req.RequirementMet())
                    {
                        enable = false; //Disable the plugin
                        log.Warn(viewRegistration.MainView.Title + " failed with value [" + req.Value + "] on Requirement [" + req.ToString() + "]");
                    }
                });

                log.Debug("View enabled=" + enable);
                viewRegistration.Enabled = enable; //Set whether or not this is disabled.
                return viewRegistration;
            }
            else
            {
                //You tell 'em boss!
                log.Warn($"{viewRegistration.MainView.Title} doesn't have a specified category!");
                log.Warn("Please ensure [PippyAttribute(\"<Category>\")] attribute is placed on the main page of that plugin!");
                return null;
            }

          
        }



        protected T GetPippyAttribute<T>(Type fromType) where T : Attribute
        {
            return (Attribute.GetCustomAttribute(fromType, typeof(T)) as T);
        }

    }
}
