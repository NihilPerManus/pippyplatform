﻿using PippyPlatform.MVVM.Service.View;

namespace PippyPlatform.MVVM.Service.PippyView
{
    internal interface IPippyViewProvier
    {
        IView Provide(IView viewRegistration);
    }
}