﻿using PippyPlatform.Attributes;
using PippyPlatform.BaseClass;
using PippyPlatform.MVVM.Service.PippyView.View;
using PippyPlatform.MVVM.Service.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PippyPlatform.MVVM.Service
{
    class PippyViewCategoryProvider : PippyClass, IPippyViewCategoryProvider
    {

      
       //category factory. use .Invoke() to create a new one!
        private Func<IPippyViewCategory> _categoryFactory;

        public PippyViewCategoryProvider( Func<IPippyViewCategory> categoryFactory)
        {
            _categoryFactory = categoryFactory;
        }


        public IEnumerable<IPippyViewCategory> GetCategories(IEnumerable<IView> ViewSource)
        {
            return ViewSource
                ?.Select(v => v.Category.Name).Distinct() //Get unique names
                ?.Select(catString => 
                {
                    //create a category object
                    var cat = _categoryFactory?.Invoke();
                    //Give the name of the category to the object
                    cat.Name = catString;
                    //place all the related views into the object and then return it
                    cat.Views = ViewSource.Where(v => v.Category.Name == catString);

                    return cat;
                });
        }

        //I've seen this somewhere before...
        protected T GetPippyAttribute<T>(Type fromType) where T : Attribute
        {
            return (Attribute.GetCustomAttribute(fromType, typeof(T)) as T);
        }

  
    }
}
