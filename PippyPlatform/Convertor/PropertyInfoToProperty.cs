﻿using PippyPlatform.MVVM.Service.PippyModule.Module;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace PippyPlatform.Convertor
{
    public class PropertyInfoToProperty : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            //This is beautiful
            //gets the propertyinfo, determines what type it is and returns that property
            IModule module = null;
            PropertyInfo property = null;

            foreach(var v in values)
            {

                if (typeof(IModule).IsAssignableFrom(v.GetType()))
                {
                  
                    module = (IModule)v;
                }

                if (v is PropertyInfo)
                {                   
                    property = (PropertyInfo)v;
                }

            }
            Console.WriteLine(property.PropertyType);

            //textboxes won't show if null. Which I guess is good for classes which I don't want to recurse through
            if(property.PropertyType == typeof(string))
                property.SetValue(module.BaseClass,"");


            return property.GetValue(module.BaseClass);

        }

      

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            Console.WriteLine("CONVERTBACK CALLED");
            return null;
        }

   
    }
}
