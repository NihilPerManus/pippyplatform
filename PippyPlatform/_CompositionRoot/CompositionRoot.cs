﻿using PippyPlatform.Attributes;
using PippyPlatform.MVVM.Service;
using PippyPlatform.MVVM.Service.AssemblyLoader;
using PippyPlatform.MVVM.Service.View;
using PippyPlatform.MVVM.ViewModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.CompilerServices;
using PippyPlatform.MVVM.Service.PippyModule.Module;
using PippyPlatform.MVVM.Service.PippyModule;
using PippyPlatform.MVVM.Model;
using PippyPlatform.MVVM.Service.PippyView.View;
using PippyPlatform.MVVM.Service.PippyView;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace PippyPlatform._CompositionRoot
{
    [PippyCategory("PLATFORM")] //Doesn't need a category but lol
    class CompositionRoot
    {
        //Instantiate the Log4Net logger
        static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [STAThread]
        static void Main() //this is run by the exe.
        {
            log.Info("Entry point executed.");
            Container container = new Container(); //create a container, this container will contain everything

            //This line is required so that instatiated classes from other modules are exactly as they were instantiated when activated by the assembly reader
            container.Options.DefaultLifestyle = Lifestyle.Singleton;
           
            //dependency injection time
            log.Debug("Begin Bootstrap");
            container.Register<MainWindow>(Lifestyle.Transient);
            container.Register<IViewModel, ViewModel>();
            Bootstrap(ref container);

            //run the application
            RunApplication(container);
            log.Debug("Main thread terminated");
        }

        private static void RunApplication(Container container)
        {
            log.Debug("Attempting to run application");
            try
            {
                App app = new App();
                //assign the viewmodel to the datacontext of the main window to link them together
                MainWindow mainWindow = container.GetInstance<MainWindow>();
                mainWindow.DataContext = container.GetInstance<IViewModel>();
                log.Info("Running App");
                //run the app
                app.Run(mainWindow);
                log.Info("App Closed!");
            }
            catch
            {

            }
        }

        //The bootstrap is where all the specific composition occurs. All the services and classes are registered here for the dependency injector to work
        private static Container Bootstrap(ref Container container)
        {
            //Get a new container, this is like a dodgy factory but it turned out I used to to get the bare essentials for this application
            Container bootStrapContainer = GetBootStrapContainer();

            //These classes are used to retrieve the applications and modules for use in the application
            container.Register<IModuleManager, ModuleManagerService>(Lifestyle.Singleton);
            container.Register<IApplicationManager, ApplicationManagerService>(Lifestyle.Singleton);

            //Load all the modules into the main container
           //create a new container for the plugins... This container is the one that dips itself ito the composition root of each application/module
            var pluginContainer = new Container();

            //ensure it holds onto each item it picks up
            pluginContainer.Options.DefaultLifestyle = Lifestyle.Singleton;

            //process both modules and applications loading them into the plugin container
            LoadAssemblyIntoContainer(ref pluginContainer, new LocalModuleFolderLoader());
            LoadAssemblyIntoContainer(ref pluginContainer, new LocalPluginFolderLoader());

            //now also register the modules into the main container. This will allow the applications to talk to the modules
            LoadAssemblyIntoContainer(ref container, new LocalModuleFolderLoader());

            //Write out all the registrations mostly for debuggin purposes
            container.GetCurrentRegistrations()
            ?.ToList()
            ?.ForEach(m =>
            {
                //Get every single registered class and print them out
                Console.WriteLine("### " + m.Registration.ImplementationType + " ###");               
                if(GetPippyAttribute<PippyModuleNameAttribute>(m.Registration.ImplementationType) != null)
                {
                    Console.WriteLine("Module: " + GetPippyAttribute<PippyModuleNameAttribute>(m.Registration.ImplementationType)?.Name);                   
                }
                else
                {
                    Console.WriteLine("No module attribute :(");
                }                       
            });


            //GET MODULES
            var modules = pluginContainer.GetCurrentRegistrations() //get every single registration that is a module
                .Where(r => GetPippyAttribute<PippyModuleNameAttribute>(r.Registration.ImplementationType) != null)
                .Select(m => {
                    Console.WriteLine("MODULE!!!!!!!!!!!!!!!!!"); // that debugging...

                    //for every module we create an item
                    IModule v = bootStrapContainer.GetInstance<Func<IModule>>()?.Invoke();
                    
                    //store an instance of that module
                    object b = m.Registration.Container.GetInstance(m.Registration.ImplementationType);
                    Console.WriteLine("Modules inner hash is " + b.GetHashCode());
                    v.BaseClass = b;


                    //This looks like it makes the above pointless but not really. We pass the object in which containes the base class
                    v = bootStrapContainer.GetInstance<IPippyModuleProvider>().Provide(v);
           
                    return v;
                });

            Console.WriteLine("MODULE COUNT => " + modules.Count());

            log.Debug("PluginContainer Registrations");
            //In the console point out the composition roots. It should be pointing to _compositeroot.Compositionroot
            pluginContainer.GetCurrentRegistrations().ToList().ForEach(t => Console.WriteLine(t.Registration.ImplementationType + "# << Page? = " + t.Registration.ImplementationType.IsSubclassOf(typeof(Page))));
            pluginContainer.GetCurrentRegistrations().ToList().ForEach(t => 
            {
                var ty = t.Registration.ImplementationType;

                if(GetPippyAttribute<PippyCategoryAttribute>(ty) == null)
                    Console.WriteLine(ty + "has No category :(");
                else
                {
                    //Console.WriteLine(t.Registration.Container.GetInstance(ty));
                }

            });


            //GET APPLICATIONS
            var views = pluginContainer.GetCurrentRegistrations()
                .Where(t => t.Registration.ImplementationType.IsSubclassOf(typeof(Page))) //Get the registrations which are pages. This is the view
                .Where(r => GetPippyAttribute<PippyCategoryAttribute>(r.Registration.ImplementationType) != null) //Get the ones that have categories
                .Select(m =>
                {
                    IView v = bootStrapContainer.GetInstance<Func<IView>>()?.Invoke();

                    //get the object from the container based on the type passed.
                    var p = m.Registration.Container.GetInstance(m.Registration.ImplementationType);
                    
                    //Is it a page?
                    if (p is Page)
                    {
                        Console.WriteLine(p + " is a Page");
                        //Instantiate the page
                        v.MainView = m.Registration.Container.GetInstance(m.Registration.ImplementationType) as Page;
                    }

                    Console.WriteLine(v.MainView);
                    //Now pass to the application provider and get a better built application
                    v = bootStrapContainer.GetInstance<IPippyViewProvier>().Provide(v);
                    Console.WriteLine(v == null);
                    return v;
                });

            //Place all the things we collected into their arrays registering the collection
            container.RegisterCollection(modules.ToArray());
            container.RegisterCollection(views.ToArray());

            //get all the pages
            var categories = bootStrapContainer.GetInstance<IPippyViewCategoryProvider>().GetCategories(views);
            //dynamically register them to the container as a collection
            container.RegisterCollection(categories);
            

            log.Info("Verifying container");
            
            log.Info("Container registration!");
            //print out the object hashes. This is used to compare registrations to ensure it is the same object and not just a newly instatiated version.
            //This was fixed with the whole singleton Lifestyle option set for the containers
            container.GetCurrentRegistrations()
                .ToList()?
                .ForEach(r => 
                {
                    //write outthe hashes. No longer important but good to have just incase
                    var hash = r.Registration.Container.GetInstance(r.Registration.ImplementationType).GetHashCode();
                    log.Debug("Registered " + r.Registration.ImplementationType + " verified! Hash = " + hash);
                    
                });
            //return the container
            return container;
        }
        private static void LoadAssemblyIntoContainer(ref Container container, IAssemblyLoaderModel loader)
        {
            Container AssemblyContainer = new Container();
           
            AssemblyContainer.Register<IAssemblyLoaderService, AssemblyLoaderService>();
            AssemblyContainer.Register(() => loader); //by ref means [Container shared] will change

            log.Info("Running " + loader + " Loader");
            //load the assemblies
            AssemblyContainer.GetInstance<IAssemblyLoaderService>().Load(ref container);
            log.Info("Assembly loader complete");
        }   
        private static Container GetBootStrapContainer()
        {

            //this is the bootstrap container. 
            //Default classes should go here. The others which could change should go in the main section

            Container bootStrapContainer = new Container();

            //Module registrations
            bootStrapContainer.Register<IPippyModuleProvider, PippyModuleProvider>(Lifestyle.Singleton);
            bootStrapContainer.RegisterSingleton<Func<IModule>>(() => new Module());
            bootStrapContainer.RegisterSingleton<Func<IModuleProperty>>(() => new ModuleProperty());
            

            //application registrations
            bootStrapContainer.RegisterSingleton<Func<IView>>(() => new PippyView());
            bootStrapContainer.Register<IPippyViewCategoryProvider, PippyViewCategoryProvider>(Lifestyle.Singleton);
            bootStrapContainer.RegisterSingleton<Func<IPippyViewCategory>>(() => new PippyViewCategory());
            bootStrapContainer.Register <IPippyViewProvier,PippyViewProvider>(Lifestyle.Singleton);
     
            //verify the registrations
            bootStrapContainer.GetCurrentRegistrations()
               .ToList()?
               .ForEach(r =>
               {
                   log.Debug("Registered " + r.Registration.ImplementationType + " verified!");
               });


            return bootStrapContainer;
        }

        //easy way to determine attribute.
        private static T GetPippyAttribute<T>(Type fromType) where T : Attribute
        {
            return (Attribute.GetCustomAttribute(fromType, typeof(T)) as T);
        }
    }
}
