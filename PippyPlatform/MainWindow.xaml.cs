﻿using MahApps.Metro.Controls;
using PippyPlatform.MVVM.ViewModel;

namespace PippyPlatform
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow(IViewModel viewModel)
        {
            InitializeComponent();
        }
    }
}
