﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.Attributes
{

    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class PippyTypeAttribute : Attribute
    {
        private string _Type;

        public string Type { get { return _Type.ToUpper(); } }

        public PippyTypeAttribute(string type)
        {
            Console.WriteLine("BOOP");
            _Type = type;
        }
    }
}
