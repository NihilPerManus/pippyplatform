﻿using System;


namespace PippyPlatform.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class PippyCategoryAttribute : Attribute
    {
        private string _Category;

        public string Category { get { return _Category.ToUpper(); } }
       
        public PippyCategoryAttribute(string category)
        {
            _Category = category;
        }
    }


}