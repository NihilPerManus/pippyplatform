﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class PippyDomainRequirementAttribute : Attribute, IRequirement
    {
        static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Value { get; set; }
        
        public PippyDomainRequirementAttribute(string domain)
        {
            Value = domain;
        }

        public bool RequirementMet()
        {
            log.Debug("Current domain: [" + Domain.GetCurrentDomain().Name.ToLower() + "]  Expected: [" + Value.ToLower() + "] => " + (Domain.GetCurrentDomain().Name.ToLower() == Value.ToLower()));
            return Domain.GetCurrentDomain().Name.ToLower() == Value.ToLower();
        }

        public override string ToString()
        {
            return "Connected to Domain";
        }

    }
}
