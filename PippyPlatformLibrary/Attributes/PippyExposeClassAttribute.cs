﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.Attributes
{
    [AttributeUsage(AttributeTargets.Class,Inherited =true)]
    public class PippyExposeClassAttribute : Attribute
    {
        
    }
}
