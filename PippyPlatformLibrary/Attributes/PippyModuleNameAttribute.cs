﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.Attributes
{
    public class PippyModuleNameAttribute : Attribute
    {
        public PippyModuleNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }
}
