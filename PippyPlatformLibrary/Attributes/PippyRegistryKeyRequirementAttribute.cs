﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    class PippyRegistryKeyRequirementAttribute : Attribute, IRequirement
    {
        private RegistryHive _baseView;
        private string _propertyName;
        private string _subPath;

        

        public PippyRegistryKeyRequirementAttribute(RegistryHive BaseKey, string subKey, string propertyName)
        {
            _baseView = BaseKey;
            _subPath = subKey;
            _propertyName = propertyName;
        }

        public string Value
        {
            get
            {
                return _baseView.ToString() + _subPath + _propertyName;
            }
        }

        public bool RequirementMet()
        {
            using (var baseKey = RegistryKey.OpenBaseKey(_baseView, RegistryView.Registry64))
            using (var subKey = baseKey.OpenSubKey(_subPath))
            {
                var val = subKey.GetValue(_propertyName);

                if (val == null)
                    return false;
                else
                    return true;            
            };
        }

        public override string ToString()
        {
            return "Has Registry Key";
        }

    }
}
