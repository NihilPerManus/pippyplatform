﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class PippyFileSystemRequirementAttribute : Attribute, IRequirement
    {
  
        string _path;
        public string Value { get { return _path; } }

        /// <summary>
        /// <para>The path to the file or directory</para>
        /// </summary>
        /// <param name="path"></param>
        public PippyFileSystemRequirementAttribute(string path)
        {
            _path = path.ToLower();
        }

        public bool RequirementMet()
        {
            return Directory.Exists(_path) || File.Exists(_path);                  
        }

        public override string ToString()
        {
            return "Has File/Folder";
        }

    }
}
