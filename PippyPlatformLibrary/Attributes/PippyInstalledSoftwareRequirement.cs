﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class PippyInstalledSoftwareRequirement : Attribute, IRequirement
    {
        static List<string> InstalledSoftware = new List<string>();

        public string Value { get; set; }

        static PippyInstalledSoftwareRequirement()
        {
            string uninstallKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            using (RegistryKey rk = Registry.LocalMachine.OpenSubKey(uninstallKey))
            {
                foreach (string skName in rk.GetSubKeyNames())
                {
                    using (RegistryKey sk = rk.OpenSubKey(skName))
                    {
                        try
                        {
                            if (sk?.GetValue("DisplayName")?.ToString() != null)
                                InstalledSoftware.Add(sk.GetValue("DisplayName").ToString());
                        }
                        catch (Exception ex)
                        { }
                    }
                }
            }
        }

        public PippyInstalledSoftwareRequirement(string software)
        {
            Value = software;
        }

        public bool RequirementMet()
        {
            return InstalledSoftware.Contains(Value);
        }
        public override string ToString()
        {
            return "Software Installed";
        }
    }
}
