﻿namespace PippyPlatform.Attributes
{
    public interface IRequirement
    {
        string Value { get; }
        bool RequirementMet();
    }
}