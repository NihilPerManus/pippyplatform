﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.Attributes
{
    public class PippyRangeAttribute : Attribute
    {
        public float Min { get; set; }
        public float Max { get; set; }
        public float Stops { get; set; }
        public PippyRangeAttribute(float min, float max, float stops)
        {
            Min = Math.Min(min, max);
            Max = Math.Max(min, max);
            Stops = stops;
        }
        public PippyRangeAttribute(float min, float max) : this(min, max, 1) { }
        
    }
}
