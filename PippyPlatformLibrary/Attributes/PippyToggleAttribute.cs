﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.Attributes
{
    public class PippyToggleAttribute : Attribute
    {
        public string TrueValue { get; private set; }
        public string FalseValue { get; private set; }
        public string NullValue { get; private set; }

        public PippyToggleAttribute(string trueValue, string falseValue)
        {
            TrueValue = trueValue;
            FalseValue = falseValue;
        }

        public PippyToggleAttribute(string trueValue, string falseValue, string nullValue) : this(trueValue,falseValue)
        {
            NullValue = nullValue;
        }
    }
}
