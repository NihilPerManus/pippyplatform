﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatform.BaseClass
{
    public class PippyClass
    {
        protected log4net.ILog log;

        public PippyClass()
        {
            log = log4net.LogManager.GetLogger(GetType());
        }

        protected DirectoryInfo GetApplicationDirectory()
        {
            var InitialNameSpace = GetType().Namespace.Split('.')[0];
            return new DirectoryInfo( Directory.GetCurrentDirectory() + @"\namespace\" + InitialNameSpace);
        }
        

    }
}
