﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PippyPlatformLibrary.Notification.Toast
{
    public interface IToastNotify : INotification
    {
        string AppID { get; set; }

        string Heading { get; set; }
        string Message { get; set; }

        FileInfo SoundFile { get; set; }

    }
}
