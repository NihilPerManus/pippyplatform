﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Toolkit.Uwp.Notifications;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace PippyPlatformLibrary.Notification.Toast
{
    public class Toast : IToastNotify
    {
        public string AppID { get ; set ; }
        public string Heading { get; set; } = "";
        public string Message { get; set; } = "";
        public FileInfo SoundFile { get; set; }

        public void Notify()
        {
            if (ValidNotification())
                SendToast();
        }

        private bool ValidNotification()
        {
            return (Heading != "" && Message != "");
        }

        private void SendToast()
        {
            ToastContent content = new ToastContent()
            {


                Visual = GetToastVisual(),
                //Actions = GetToastActionsCustom(),
                Audio = GetToastAudio(),
            };

            XmlDocument d = new XmlDocument();
            d.LoadXml(content.GetContent());


            var toast = new ToastNotification(d);

            ToastNotificationManager.CreateToastNotifier("com.PoweroutageTester.Pippy").Show(toast);
        }
        private ToastAudio GetToastAudio()
        {
            return null; //no sound?
        }

        private IToastActions GetToastActionsCustom()
        {
            return null;
        }

        private ToastVisual GetToastVisual()
        {
            return new ToastVisual()
            {
                BindingGeneric = GetBindingGeneric()
            };
        }
        private ToastBindingGeneric GetBindingGeneric()
        {
            return new ToastBindingGeneric()
            {
                Children =
                {
                    new AdaptiveText()
                    {
                        Text = Heading
                    },

                     new AdaptiveText()
                    {
                        Text = Message
                    },


                }
            };
        }

    }
}
